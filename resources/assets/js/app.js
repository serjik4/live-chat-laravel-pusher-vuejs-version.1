
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('message-component', require('./components/MessageComponent.vue'));

import Toaster from 'v-toaster';
import 'v-toaster/dist/v-toaster.css'

Vue.use(Toaster, {timeout: 5000})

import Vue from 'vue';

import VueChatScroll from 'vue-chat-scroll';

Vue.use(VueChatScroll);

const app = new Vue({
    el: '#app',
    data: {
        message: '',
        chat: {
            messages: [],
            users: [],
            colors: [],
            time: []
        },
        typing: false,
        numberOfUsers: 0
    },
    watch: {
        message() {
            Echo.private('chat')
                .whisper('typing', {
                    name: this.message
                });
        }
    },
    methods : {
        send(){
            if(this.message.length != 0){
                this.chat.users.push('you');
                this.chat.colors.push('success');
                this.chat.time.push(this.getTime());
                axios.post('/send', {
                    message: this.message,
                    chat: this.chat
                })
                    .then(response => {
                        //console.log(response);
                        this.message = '';
                })
                    .catch(error => {
                        console.log(error);
                });
                this.chat.messages.push(this.message);
                //console.log(this.chat.user);
            }
        },
        getTime() {
            let time = new Date();
            return time.getHours()+':'+time.getMinutes();
        },
        getOldMessages() {
            axios.post('/getOldMessages').then(response => {
                if(response.data != '') {
                    this.chat = response.data;
                }
            }).catch(error => {
                console.log(error);
            });
        },
        deleteSession() {
            axios.post('/deleteSession').then(response => {
                this.$toaster.success('Chat history is deleted');
            });
        }
    },
    mounted(){
        this.getOldMessages();
        Echo.private('chat').listen('ChatEvent', (response) => {
            //console.log(response);
            this.chat.messages.push(response.message);
            this.chat.users.push(response.user);
            this.chat.colors.push('warning');
            this.chat.time.push(this.getTime());
            axios.post('saveToSession', {
                chat: this.chat
            }).then(response => {}).catch(error => {console.log(error)});
        }).listenForWhisper('typing', (e) => {
            if(e.name != '') {
                this.typing = true;
            } else {
                this.typing = false;
            }
        });
        Echo.join(`chat`)
            .here((users) => {
                //console.log(users);
                this.numberOfUsers = users.length;
            })
            .joining((user) => {
                this.numberOfUsers += 1;
                //console.log(user);
                this.$toaster.success(user.name + ' is joined the chat room');
            })
            .leaving((user) => {
                this.numberOfUsers -= 1;
                this.$toaster.warning(user.name + ' is lived the chat room');
            });

    }
});
