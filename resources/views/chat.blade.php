<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
        .list-group{
            overflow-y: scroll;
            height: 200px;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row" id="app">
            <div class="offset-2 col-8">
                <li class="list-group-item active">Chat Room <span class="badge badge-pill badge-danger">@{{ numberOfUsers }}</span></li>
                <div class="badge badge-pill badge-primary" v-if="typing">typing...</div>
                <ul class="list-group" v-chat-scroll>
                    <message-component :key="message.index" v-for="(message, index) in chat.messages" :time="chat.time[index]" :color="chat.colors[index]" :user="chat.users[index]">@{{ message }}</message-component>
                </ul>
                <input type="text" v-model="message" v-on:keyup.enter="send" class="form-control" placeholder="Type your message">
                <br />
                {{--<a href="#" class="btn btn-primary btn-sm" @click.prevent="deleteSession">Delete chat history</a>--}}
            </div>
        </div>
    </div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>