<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\ChatEvent;
use App\User;

class ChatController extends Controller
{
    public function chat()
    {
        return view('chat');
    }

    public function send(Request $request)
    {
        $user = Auth::user();
        $this->saveToSession($request->chat);
        event(new ChatEvent($request->get('message'), $user->name));
    }

    public function saveToSession($chat)
    {
        session()->put('chat', $chat);
    }

    public function getOldMessages()
    {
        return session('chat');
    }

    public function deleteSession()
    {
        session()->forget('chat');
    }
}
