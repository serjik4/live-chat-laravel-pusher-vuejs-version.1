<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chat', 'ChatController@chat')->middleware('auth');
Route::post('/send', 'ChatController@send')->middleware('auth');
Route::post('/saveToSession', 'ChatController@saveToSession')->middleware('auth');
Route::post('/deleteSession', 'ChatController@deleteSession')->middleware('auth');
Route::post('/getOldMessages', 'ChatController@getOldMessages')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/check', function (){
    return session('chat');
});